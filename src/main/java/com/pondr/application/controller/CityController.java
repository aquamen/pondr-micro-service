package com.pondr.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pondr.application.model.City;
import com.pondr.application.service.ICityService;

@RestController
public class CityController {
	
	@Autowired
    ICityService cityService;

	@GetMapping("/showCities")
	public String findCities(Model model) {
	    
	    List<City> cities = (List<City>) cityService.findAll();
	    
	    model.addAttribute("cities", cities);
	    
	    StringBuilder sb = new StringBuilder();
	    for(City c : cities) {
	    	
	    	
	    	System.out.println(c.getName());
	    	
	    }
	    return "showCities " + sb.toString();
	}
}
