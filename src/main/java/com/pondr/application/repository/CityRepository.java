package com.pondr.application.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pondr.application.model.City;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {

}