package com.pondr.application.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.pondr.application.model.User;

@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {

	 public User findByEmail(String email);

}
