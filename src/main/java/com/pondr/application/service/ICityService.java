package com.pondr.application.service;

import java.util.List;

import com.pondr.application.model.City;

public interface ICityService {

    public List<City> findAll();
}
