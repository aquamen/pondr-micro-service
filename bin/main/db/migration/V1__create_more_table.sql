create table migration_tbl2(
   migration_id INT NOT NULL AUTO_INCREMENT,
   migration_title VARCHAR(100) NOT NULL,
   migration_author VARCHAR(40) NOT NULL,
   submission_date DATE,
   PRIMARY KEY ( migration_id )
);